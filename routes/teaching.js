var express = require('express');
var router = express.Router();

var controller = require('../controllers/teachingController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('teaching', { title: 'Amaury DUBOIS' });
});


router.get('/:topic/:lesson' , controller.lesson);
router.get('/:topic/:lesson/file/:file' , controller.ressource);

module.exports = router;
