
const courses = {
 gal : {
  ennonce : {titleAbr : "GAL", title : "Génie et Architecture Logiciel", view: "courses/gal" },
 },
 algo : {
    ennonce :{titleAbr : "Algo", title : "Introduction à l'algorithmie", view: "courses/algo/condense" }
   },
 xml :{
     introduction : {titleAbr : "Introduction", title : "eXtansible Markup Language : XML" , view: "courses/xml/introduction"},
  xml : {titleAbr : "XML",title : "language XML" , view: "courses/xml/xml"},
  dtd : {titleAbr: "DTD",title : "Documents type definitions (DTD) " , view: "courses/xml/dtd"},
  xmlschema : {titleAbr : "XML Schemas",title : "XMLSchemas" , view: "courses/xml/xml_schema"},
  xpath : {titleAbr : "XPath", title : "Xml Path Language" , view: "courses/xml/xpath"},
  xslt : {titleAbr : "XSLT", title : "XSLT" , view: "courses/xml/xslt"},
  svg : {titleAbr : "SVG", title : "SVG" , view: "courses/xml/svg"},
  json : {titleAbr : "JSON", title : "JavaScript Object Notation (JSON)" , view: "courses/xml/json"},
  examen : {titleAbr : "Examen", title : "Examen service web" , view: "courses/xml/examen"},
  tp1 : {titleAbr : "TP1 ",title : "TP1 : XML" , view: "courses/xml/tp/tp1"},
  tp2 : {titleAbr : "TP2 ",title : "TP2 : DTD" , view: "courses/xml/tp/tp2"},
  tp3 : {titleAbr : "TP3 ",title : "TP3 : XSD" , view: "courses/xml/tp/tp3"},
  tp4 : {titleAbr : "TP4 ",title : "TP4 : XPath" , view: "courses/xml/tp/tp4"},
  tp5 : {titleAbr : "TP5 ",title : "TP5 : XSLT" , view: "courses/xml/tp/tp5"},
  tp6 : {titleAbr : "TP6 ",title : "TP6 : SVG" , view: "courses/xml/tp/tp6"},
  tp7 : {titleAbr : "TP7 ",title : "TP7 : JSON" , view: "courses/xml/tp/tp7"}
 }
};

module.exports = {courses};
